const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const errorController = require('./controllers/error');
var User = require('./models/user');

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
var User = mongoose.model('user');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
  User.findById('5bab316ce0a7c75f783cb8a8')
    .then(user => {
      req.user = user;
      next();
      
    })
    .catch(err => console.log(err));
});

app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

mongoose
  .connect(
    'mongodb+srv://adibaiman:Codelyoko123@cluster0.fayzrlw.mongodb.net/?retryWrites=true&w=majority'
  )
  .then(result => {
    User.findOne().then(user => {
        var User = new user({
          name: 'Max',
          email: 'max@test.com',
          cart: {
            items: []
          }
        });
        user.save();
      });
    app.listen(3000)
  })
  .catch(err => {
    console.log(err);
  });

  var UserSchema = new Schema({
    email: {
      type: String,
      unique: true,
      lowercase: true
    },
    password: String });
     module.exports =  mongoose.model('User', UserSchema) 