const person = {
    name : 'Adib',
    age: 18,
    greet() {
        console.log('Hi, I am ' + this);
    }
};


const copiedPerson = {...person};
console.log(copiedPerson);



const hobbies = ['Sports', 'Cooking'];
// // for (let hobby of hobbies) {
// //     console.log(hobby)
// // }
// // console.log(hobbies.map(hobby => 'Hobby:' + hobby));
// // console.log(hobbies);
const copiedArray = [...hobbies];
console.log(copiedArray);


const toArray = (...args) => {
    return args;
};

console.log(toArray(1, 2, 3, 4 ,5, 6));
