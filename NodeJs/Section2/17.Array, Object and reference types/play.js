const person = {
    name : 'Adib',
    age: 18,
    greet() {
        console.log('Hi, I am ' + this);
    }
};

const hobbies = ['Sports', 'Cooking'];
// for (let hobby of hobbies) {
//     console.log(hobby)
// }
// console.log(hobbies.map(hobby => 'Hobby:' + hobby));
// console.log(hobbies);
hobbies.push('Programming');
console.log(hobbies);

