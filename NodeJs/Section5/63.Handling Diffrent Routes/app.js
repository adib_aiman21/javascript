const http = require('http');

const express = require('express');

const app = express();

app.use('/', (req, res, next) => {
    console.log('This is always run');
    next();
});

app.use('/add-product', (req, res, next) => {
    console.log('In the middleware');
    res.send('The add product is here');
});

app.use('/',(req, res, next) => {
    console.log('In another middleware');
    res.send('<h1>Hello from Ethernet</h1>');
});

// const server = http.createServer(app);

// server.listen(3000);

app.listen(3000);